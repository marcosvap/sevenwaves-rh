package com.sevenwavesrh.security;

import static java.util.Collections.emptyList;

import com.sevenwaves.services.api.common.newmodel.UserAuthRH;
import com.sevenwaves.services.api.common.repository.UserAuthRHRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	private UserAuthRHRepository userAuthRHRepository;

	public UserDetailsServiceImpl(UserAuthRHRepository userAuthRHRepository) {
		this.userAuthRHRepository = userAuthRHRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserAuthRH user = userAuthRHRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}

		CustomUserDetails customUserDetail=new CustomUserDetails();
		customUserDetail.setUser(user);

		return customUserDetail;

	}
}