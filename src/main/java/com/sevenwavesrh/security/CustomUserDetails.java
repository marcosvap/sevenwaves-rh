package com.sevenwavesrh.security;

import com.sevenwaves.services.api.common.newmodel.UserAuthRH;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

public class CustomUserDetails implements UserDetails {
	private static final long serialVersionUID = 1L;
	private UserAuthRH user;

	Set<GrantedAuthority> authorities=null;

	public UserAuthRH getUser() {
		return user;
	}

	public void setUser(UserAuthRH user) {
		this.user = user;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities)
	{
		this.authorities=authorities;
	}

	public String getPassword() {
		return user.getPassword();
	}

	public String getUsername() {
		return user.getUsername();
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}
}