package com.sevenwavesrh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.model.dto.User;
import com.sevenwaves.services.api.common.newmodel.Access;
import com.sevenwaves.services.api.common.repository.AccessRepository;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/access")
public class AccessController {

	@Autowired
	private AccessRepository accessRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveAccess(@RequestBody User user,
			@RequestParam(name = "corporate", required = true) String corporate) {

		Access access = this.accessRepository.findByCorporate(corporate);

		access.getUsers().add(user);

		access = this.accessRepository.save(access);

		return new ResponseEntity<Object>(access, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateAccess(@RequestBody Access access, @PathVariable(name = "id") String id) {

//		Request mainRequest = this.accessRepository.findByCorporate(corporate);
//
//		List<com.sevenwaves.services.api.common.model.dto.Request> newRequests = new ArrayList<>();
//		
//		mainRequest.getRequest().forEach(req -> {
//			if() {
//				
//			}
//		});

		access = this.accessRepository.save(access);

		return new ResponseEntity<Object>(access, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteAccess(@PathVariable(name = "id") String id) {
		this.accessRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllAccess(@RequestParam(name = "corporate", required = true) String corporate) {
		Access access = this.accessRepository.findByCorporate(corporate);

		return new ResponseEntity<Object>(access, HttpStatus.OK);
	}

}