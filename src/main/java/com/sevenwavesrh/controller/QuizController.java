package com.sevenwavesrh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sevenwaves.services.api.common.newmodel.Quiz;
import com.sevenwaves.services.api.common.repository.QuizRepository;
import com.sevenwavesrh.model.enuns.QuizFilterType;

@Controller
//@EnableAutoConfiguration
@RequestMapping("/quiz")
public class QuizController {

	@Autowired
	private QuizRepository quizRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveQuiz(@RequestBody Quiz quiz) {
		quiz = this.quizRepository.save(quiz);

		return new ResponseEntity<Object>(quiz, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateQuiz(@RequestBody Quiz quiz, @PathVariable(name = "id") String id) {
		quiz = this.quizRepository.save(quiz);

		return new ResponseEntity<Object>(quiz, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteQuiz(@PathVariable(name = "id") String id) {
		this.quizRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllQuizByFilter(
			@RequestParam(name = "filterType", required = false) QuizFilterType filterType,
			@RequestParam(name = "filter", required = false) String filter) {
		List<Quiz> quizzes = new ArrayList<Quiz>();

		quizzes = this.quizRepository.findAll();

		if (filterType != null && filter != null && !filter.isEmpty()) {
			quizzes = quizzes.stream().map(quizz -> getQuizByFilter(quizz, filter, filterType))
					.collect(Collectors.toList()).stream().filter(quiz -> quiz != null).collect(Collectors.toList());
		}

		return new ResponseEntity<Object>(quizzes, HttpStatus.OK);
	}

	private Quiz getQuizByFilter(Quiz quizz, String filter, QuizFilterType filterType) {
		if (filterType.equals(QuizFilterType.TITLE)) {
			if (quizz.getTitle().toLowerCase().contains(filter.toLowerCase())) {
				return quizz;
			} else {
				return null;
			}
		} else if (filterType.equals(QuizFilterType.AREA)) {
			return quizz;
		} else if (filterType.equals(QuizFilterType.PERSON)) {
			return quizz;
		} else {
			return quizz;
		}
	}

}