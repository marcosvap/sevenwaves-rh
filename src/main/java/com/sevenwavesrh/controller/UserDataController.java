package com.sevenwavesrh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.newmodel.UserData;
import com.sevenwaves.services.api.common.repository.UserDataRepository;
import com.sevenwavesrh.model.enuns.UserFilterType;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/user")
public class UserDataController {

	@Autowired
	private UserDataRepository userDataRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveUser(@RequestBody UserData user) {

		user = this.userDataRepository.save(user);

		return new ResponseEntity<Object>(user, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateUser(@RequestBody UserData user, @PathVariable(name = "id") String id) {

		user = this.userDataRepository.save(user);

		return new ResponseEntity<Object>(user, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteUser(@PathVariable(name = "id") String id) {
		this.userDataRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllUserByFilter(
			@RequestParam(name = "filterType", required = false) UserFilterType filterType,
			@RequestParam(name = "filter", required = false) String filter) {
		List<UserData> users = new ArrayList<UserData>();

		users = this.userDataRepository.findAll();

		if (filterType != null && filter != null && !filter.isEmpty()) {
			users = users.stream().map(user -> getUserByFilter(user, filter, filterType)).collect(Collectors.toList())
					.stream().filter(user -> user != null).collect(Collectors.toList());
		}

		return new ResponseEntity<Object>(users, HttpStatus.OK);

	}

	private UserData getUserByFilter(UserData user, String filter, UserFilterType filterType) {

		if (filterType.equals(UserFilterType.NAME)) {
			if (user.getName().toLowerCase().contains(filter.toLowerCase())) {
				return user;
			} else {
				return null;
			}
		} else if (filterType.equals(UserFilterType.REGISTRATION)) {
			return user;
		} else if (filterType.equals(UserFilterType.EMAIL)) {
			if (user.getEmail().toLowerCase().contains(filter.toLowerCase())) {
				return user;
			} else {
				return user;
			}
		} else if (filterType.equals(UserFilterType.AREA)) {
			return user;
		} else if (filterType.equals(UserFilterType.PERMISSIONS)) {
			return user;
		} else {
			return user;
		}

	}

}