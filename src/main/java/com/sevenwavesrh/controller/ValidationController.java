package com.sevenwavesrh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.newmodel.Request;
import com.sevenwaves.services.api.common.repository.RequestRepository;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/request")
public class ValidationController {

	@Autowired
	private RequestRepository requestRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveRequest(@RequestBody com.sevenwaves.services.api.common.model.dto.Request request,
			@RequestParam(name = "corporate", required = true) String corporate) {

		Request mainRequest = this.requestRepository.findByCorporate(corporate);

		mainRequest.getRequest().add(request);

		mainRequest = this.requestRepository.save(mainRequest);

		return new ResponseEntity<Object>(mainRequest, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateRequest(@RequestBody Request request, @PathVariable(name = "id") String id) {

//		Request mainRequest = this.requestRepository.findByCorporate(corporate);
//
//		List<com.sevenwaves.services.api.common.model.dto.Request> newRequests = new ArrayList<>();
//		
//		mainRequest.getRequest().forEach(req -> {
//			if() {
//				
//			}
//		});

		request = this.requestRepository.save(request);

		return new ResponseEntity<Object>(request, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteRequest(@PathVariable(name = "id") String id) {
		this.requestRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllRequest(@RequestParam(name = "corporate", required = true) String corporate) {
		Request request = this.requestRepository.findByCorporate(corporate);

		return new ResponseEntity<Object>(request, HttpStatus.OK);
	}

}