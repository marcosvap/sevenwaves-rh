package com.sevenwavesrh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.newmodel.Squad;
import com.sevenwaves.services.api.common.repository.SquadRepository;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/squad")
public class SquadController {

	@Autowired
	private SquadRepository squadRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveSquad(@RequestBody com.sevenwaves.services.api.common.model.dto.Squad squad,
			@RequestParam(name = "corporate", required = true) String corporate) {

		Squad mainSquad = this.squadRepository.findByCorporate(corporate);

		mainSquad.getSquads().add(squad);

		mainSquad = this.squadRepository.save(mainSquad);

		return new ResponseEntity<Object>(mainSquad, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateSquad(@RequestBody Squad squad, @PathVariable(name = "id") String id) {

//		Request mainRequest = this.squadRepository.findByCorporate(corporate);
//
//		List<com.sevenwaves.services.api.common.model.dto.Request> newRequests = new ArrayList<>();
//		
//		mainRequest.getRequest().forEach(req -> {
//			if() {
//				
//			}
//		});

		squad = this.squadRepository.save(squad);

		return new ResponseEntity<Object>(squad, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteSquad(@PathVariable(name = "id") String id) {
		this.squadRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllSquad(@RequestParam(name = "corporate", required = true) String corporate) {
		Squad squad = this.squadRepository.findByCorporate(corporate);

		return new ResponseEntity<Object>(squad, HttpStatus.OK);
	}

}