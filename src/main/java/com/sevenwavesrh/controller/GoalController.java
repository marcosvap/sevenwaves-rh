package com.sevenwavesrh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.newmodel.GoalRecommendation;
import com.sevenwaves.services.api.common.repository.GoalRecommendationRepository;
import com.sevenwavesrh.model.enuns.JourneyFilterType;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/goal")
public class GoalController {

	@Autowired
	private GoalRecommendationRepository goalRecommendationRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveGoal(@RequestBody GoalRecommendation goal) {

		goal = this.goalRecommendationRepository.save(goal);

		return new ResponseEntity<Object>(goal, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateGoal(@RequestBody GoalRecommendation goal, @PathVariable(name = "id") String id) {

		goal = this.goalRecommendationRepository.save(goal);

		return new ResponseEntity<Object>(goal, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteGoal(@PathVariable(name = "id") String id) {
		this.goalRecommendationRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllGoalByFilter(
			@RequestParam(name = "filterType", required = false) JourneyFilterType filterType,
			@RequestParam(name = "filter", required = false) String filter) {
		List<GoalRecommendation> goals = new ArrayList<GoalRecommendation>();

		goals = this.goalRecommendationRepository.findAll();

		if (filterType != null && filter != null && !filter.isEmpty()) {
			goals = goals.stream().map(goal -> getGoalByFilter(goal, filter, filterType)).collect(Collectors.toList())
					.stream().filter(goal -> goal != null).collect(Collectors.toList());
		}

		return new ResponseEntity<Object>(goals, HttpStatus.OK);

	}

	private GoalRecommendation getGoalByFilter(GoalRecommendation goal, String filter, JourneyFilterType filterType) {

		if (filterType.equals(JourneyFilterType.CATEGORY)) {
			return goal;
		} else if (filterType.equals(JourneyFilterType.NAME)) {
			if (goal.getName().toLowerCase().contains(filter.toLowerCase())) {
				return goal;
			} else {
				return null;
			}
		} else {
			return goal;
		}

	}

}