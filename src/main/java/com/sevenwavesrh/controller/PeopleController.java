package com.sevenwavesrh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.newmodel.User;
import com.sevenwaves.services.api.common.repository.UserRepository;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/person")
public class PeopleController {

	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveUser(@RequestBody com.sevenwaves.services.api.common.model.dto.User user,
			@RequestParam(name = "corporate", required = true) String corporate) {

		User userMain = this.userRepository.findByCorporate(corporate);

		userMain.getUsers().add(user);

		userMain = this.userRepository.save(userMain);

		return new ResponseEntity<Object>(userMain, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable(name = "id") String id) {

//		Request mainRequest = this.userRepository.findByCorporate(corporate);
//
//		List<com.sevenwaves.services.api.common.model.dto.Request> newRequests = new ArrayList<>();
//		
//		mainRequest.getRequest().forEach(req -> {
//			if() {
//				
//			}
//		});

		user = this.userRepository.save(user);

		return new ResponseEntity<Object>(user, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteUser(@PathVariable(name = "id") String id) {
		this.userRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllUser(@RequestParam(name = "corporate", required = true) String corporate) {
		User user = this.userRepository.findByCorporate(corporate);

		return new ResponseEntity<Object>(user, HttpStatus.OK);
	}

}