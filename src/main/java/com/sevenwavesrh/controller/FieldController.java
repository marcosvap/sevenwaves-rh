package com.sevenwavesrh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.newmodel.Department;
import com.sevenwaves.services.api.common.repository.FieldRepository;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/field")
public class FieldController {

	@Autowired
	private FieldRepository fieldRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveField(@RequestBody com.sevenwaves.services.api.common.model.dto.Department department,
			@RequestParam(name = "corporate", required = true) String corporate) {

		Department dep = this.fieldRepository.findByCorporate(corporate);

		dep.getDepartments().add(department);

		dep = this.fieldRepository.save(dep);

		return new ResponseEntity<Object>(dep, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateField(@RequestBody Department dep, @PathVariable(name = "id") String id) {

//		Request mainRequest = this.fieldRepository.findByCorporate(corporate);
//
//		List<com.sevenwaves.services.api.common.model.dto.Request> newRequests = new ArrayList<>();
//		
//		mainRequest.getRequest().forEach(req -> {
//			if() {
//				
//			}
//		});

		dep = this.fieldRepository.save(dep);

		return new ResponseEntity<Object>(dep, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteField(@PathVariable(name = "id") String id) {
		this.fieldRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllField(@RequestParam(name = "corporate", required = true) String corporate) {
		Department dep = this.fieldRepository.findByCorporate(corporate);

		return new ResponseEntity<Object>(dep, HttpStatus.OK);
	}

}