package com.sevenwavesrh.controller;

import com.sevenwaves.services.api.common.newmodel.Department;
import com.sevenwaves.services.api.common.newmodel.Feed;
import com.sevenwaves.services.api.common.repository.FeedRepository;
import com.sevenwaves.services.api.common.repository.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/content")
public class ContentController {

	@Autowired
	private FeedRepository feedRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveFeed(@RequestBody Feed feed) {
		this.feedRepository.save(feed);

		return new ResponseEntity<Object>(feed, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateFeed(@RequestBody Feed feed) {
		this.feedRepository.save(feed);

		return new ResponseEntity<Object>(feed, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteFeed(@PathVariable(name = "id") String id) {
		this.feedRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllFeed(@RequestParam(name = "corporate", required = true) String corporate) {
		List<Feed> feeds = this.feedRepository.findByCorporate(corporate);

		return new ResponseEntity<Object>(feeds, HttpStatus.OK);
	}

}