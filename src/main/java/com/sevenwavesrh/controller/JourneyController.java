package com.sevenwavesrh.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sevenwaves.services.api.common.newmodel.Journey;
import com.sevenwaves.services.api.common.repository.JourneyRepository;
import com.sevenwavesrh.model.enuns.JourneyFilterType;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/journey")
public class JourneyController {

	@Autowired
	private JourneyRepository journeyRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveJourney(@RequestBody Journey journey) {

		journey = this.journeyRepository.save(journey);

		return new ResponseEntity<Object>(journey, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateJourney(@RequestBody Journey journey, @PathVariable(name = "id") String id) {

		journey = this.journeyRepository.save(journey);

		return new ResponseEntity<Object>(journey, HttpStatus.OK);

	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteJourney(@PathVariable(name = "id") String id) {
		this.journeyRepository.delete(id);
	}

	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllJourneyByFilter(
			@RequestParam(name = "filterType", required = false) JourneyFilterType filterType,
			@RequestParam(name = "filter", required = false) String filter) {
		List<Journey> journeys = new ArrayList<Journey>();

		journeys = this.journeyRepository.findAll();

		if (filterType != null && filter != null && !filter.isEmpty()) {
			journeys = journeys.stream().map(joruney -> getJourneyByFilter(joruney, filter, filterType))
					.collect(Collectors.toList()).stream().filter(journey -> journey != null)
					.collect(Collectors.toList());
		}

		return new ResponseEntity<Object>(journeys, HttpStatus.OK);

	}

	private Journey getJourneyByFilter(Journey journey, String filter, JourneyFilterType filterType) {

		if (filterType.equals(JourneyFilterType.CATEGORY)) {
			return journey;
		} else if (filterType.equals(JourneyFilterType.NAME)) {
			if (journey.getName().toLowerCase().contains(filter.toLowerCase())) {
				return journey;
			} else {
				return null;
			}
		} else {
			return journey;
		}

	}

}