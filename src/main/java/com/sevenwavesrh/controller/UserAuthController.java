package com.sevenwavesrh.controller;

import com.sevenwaves.services.api.common.model.dto.User;
import com.sevenwaves.services.api.common.newmodel.Access;
import com.sevenwaves.services.api.common.newmodel.UserAuthRH;
import com.sevenwaves.services.api.common.repository.AccessRepository;
import com.sevenwaves.services.api.common.repository.UserAuthRHRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//@EnableAutoConfiguration
@RequestMapping("/userAuth")
public class UserAuthController {

	@Autowired
	private UserAuthRHRepository userAuthRHRepository;

	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> saveAccess(@RequestBody UserAuthRH user) {
		this.userAuthRHRepository.save(user);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}