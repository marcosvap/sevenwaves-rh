package com.sevenwavesrh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = "com.sevenwaves.services.api.common.repository")
@SpringBootApplication
public class SevenWavesRHApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SevenWavesRHApiApplication.class, args);
	}
}
