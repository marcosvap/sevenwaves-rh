package com.sevenwavesrh.model.enuns;

public enum FieldFilterType {
	CATEGORY, NAME, REGISTRATION
}