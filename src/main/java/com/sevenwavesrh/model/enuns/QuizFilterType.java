package com.sevenwavesrh.model.enuns;

public enum QuizFilterType {
	TITLE, AREA, PERSON, REGISTRATION
}