package com.sevenwavesrh.model.enuns;

public enum JourneyFilterType {
	CATEGORY, NAME, REGISTRATION
}