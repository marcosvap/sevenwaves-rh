package com.sevenwavesrh.model.enuns;

public enum SquadFilterType {
	CATEGORY, NAME, REGISTRATION
}