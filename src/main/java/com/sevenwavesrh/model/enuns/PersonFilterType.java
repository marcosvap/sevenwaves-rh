package com.sevenwavesrh.model.enuns;

public enum PersonFilterType {
	CATEGORY, NAME, REGISTRATION
}