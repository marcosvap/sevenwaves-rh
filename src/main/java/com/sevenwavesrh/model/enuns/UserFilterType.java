package com.sevenwavesrh.model.enuns;

public enum UserFilterType {
	NAME, REGISTRATION, EMAIL, AREA, PERMISSIONS, ROLE
}